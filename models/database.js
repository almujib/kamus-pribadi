const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
var md = require('markdown-it')();

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

const markdown = string => md.render(string);

const fetch = (shuffle=true) => {
    const data = JSON.parse(fs.readFileSync("./database/data.json").toString());
    let result = shuffle ? shuffleArray(data) : data;
    return result;
}

const dlt = id => {
    const data = fetch();
    let result = [];
    for (const element of data) {
        if (element.id != id) result.push(element);
    }
    fs.writeFileSync('./database/data.json', JSON.stringify(result));
}

const add = (title, type, tags, align, content) => {
    const id = uuidv4();
    const new_data = {
        id: id,
        title: title,
        type: type,
        tags: tags.split(","),
        align: (align == "on") ? "right" : "left",
        content: content
    }
    const data = fetch();
    data.push(new_data);
    fs.writeFileSync('./database/data.json', JSON.stringify(data));
    return id;
}

const update = (id, title, type, tags, align, content) => {
    const new_data = {
        id: id,
        title: title,
        type: type,
        tags: tags.split(","),
        align: (align == "on") ? "right" : "left",
        content: content
    }
    console.log(new_data)
    const data = fetch();
    for (let i = 0; i < data.length; i++) {
        if (data[i].id == id) data[i] = new_data ;
    }
    fs.writeFileSync('./database/data.json', JSON.stringify(data));
}

const fetch_by_id = id => {
    const data = fetch();
    for (const element of data) if (element.id == id) return element;
    return undefined; 
}

const fetch_by_tag = tag => {
    const data = fetch();
    let result = [];
    for (const element of data) {
        if (element.tags.includes(tag)) result.push(element);
    }
    return result;
}

const fetch_by_type = type => {
    const data = fetch();
    let result = [];
    for (const element of data) {
        if (element.type.includes(type)) result.push(element);
    }
    return result;
}

module.exports = {
    add,
    dlt,
    update,
    markdown,
	fetch,
    fetch_by_id,
    fetch_by_tag,
    fetch_by_type
}