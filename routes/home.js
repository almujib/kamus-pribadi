var express = require('express');
var router = express.Router();

const database = require("../models/database");

/* GET home page. */
router.get('/', function(req, res, next) {
  const context = req.context;
  context.list = database.fetch();
  res.render('pages/home/index', context);
});

router.get('/add', function(req, res, next) {
  const context = req.context;
  context.page_title = "Add";
  res.render('pages/home/add', context);
});

router.get('/delete', function(req, res, next) {
  database.dlt(req.query.id);
  res.redirect("/");
});

router.post('/add', function(req, res, next) {
  const id = database.add(req.body.title, req.body.type, req.body.tags, req.body.align, req.body.content);
  res.redirect('/home/data?id=' + id);
});

router.get('/data', function(req, res, next) {
  const context = req.context;
  context.page_title = "Data";
  context.data = database.fetch_by_id(req.query.id);
  let buffer = {
    string: context.data.content,
    markdown: database.markdown(context.data.content)
  }
  context.data.content = buffer;
  res.render('pages/home/data', context);
});

router.post('/data', function(req, res, next) {
  database.update(req.query.id, req.body.title, req.body.type, req.body.tags, req.body.align, req.body.content);
  res.redirect("/");
});

router.get('/informations', function(req, res, next) {
  const context = req.context;
  context.page_title = "Informations";
  res.render('pages/home/informations', context);
});

router.get('/tag', function(req, res, next) {
  const context = req.context;
  context.page_title = "Tag by " + req.query.tag;
  context.list = database.fetch_by_tag(req.query.tag);
  res.render('pages/home/index', context);
});

router.get('/type', function(req, res, next) {
  const context = req.context;
  context.page_title = "Type by " + req.query.type;
  context.list = database.fetch_by_type(req.query.type);
  res.render('pages/home/index', context);
});

router.get('/404', function(req, res, next) {
  const context = req.context;
  context.page_title = "404";
  res.render('pages/home/404', context);
});

module.exports = router;
